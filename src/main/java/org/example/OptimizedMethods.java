package org.example;

/**
 * 最优化方法
 */
public class OptimizedMethods {
    /**
     * 复合形法
     */
    public void compoundOptimization() {
        // 收敛精度
        final double epsilon = 1e-4;
        //顶点的向量维数
        final int dimensionCount = 3;
        // 复合形顶点个数
        final int pointsCount = 6;
        double[][] x = new double[pointsCount][dimensionCount];
        // 指定一个在可行域内的初始点x1
        x[0] = new double[]{22, 52, 5};
        // 初始下限值
        double[] a = new double[]{0, 0, 0};
        // 初始上限值
        double[] b = new double[]{0.5, 0.5, 0.5};
        // 可行点的中心
        double[] xC = new double[dimensionCount];
        // 反射点
        double[] xR = new double[dimensionCount];
        // 记录最终迭代次数
        int iterations = 0;
        while (true) {
            boolean flag = true;
            // 1 随机生成剩余k-1个点
            getOtherPoints(dimensionCount, pointsCount, x, a, b);
            // 2 找出可行点的个数L，并把可行点放在前L个位置上
            int feasiblePointsCount = getFeasiblePointCountAndPutHead(dimensionCount, pointsCount, x);
            System.out.printf("feasiblePointsCount = %d\n", feasiblePointsCount);
            // 3 把前L个可行点按目标函数值降序排列
            setElementsToDesOrderByTargetValue(dimensionCount, feasiblePointsCount, x);
            // 4 求可行点中心
            getCenterPointOfFeasiblePoints(dimensionCount, x, xC, feasiblePointsCount);
            // 5 将不可行点可行化
            setNoFeasiblePointsToFeasible(dimensionCount, pointsCount, x, xC, feasiblePointsCount);
            while (flag) {
                boolean flag2 = true;
                // 6 将可行点按目标函数值从大到小排序
                setElementsToDesOrderByTargetValue(dimensionCount, pointsCount, x);
                // 7 求收敛条件
                double delta = 0;
                for (int i = 0; i < pointsCount; i++) {
                    delta += Math.pow((f(x[i]) - f(x[pointsCount - 1])), 2);
                }
                delta = Math.sqrt(1.0 / (pointsCount - 1) * delta);
                iterations++;
                System.out.printf("delta = %.10f\n", delta);
                if (delta < epsilon) {

                    System.out.printf("迭代次数：%d\n求得约束最优点为：（", iterations);
                    for (int i = 0; i < dimensionCount; i++) {
                        System.out.printf("%.5f, ", x[pointsCount - 1][i]);
                    }
                    System.out.printf(")\t目标函数的最优解为：%.5f\n\n", f(x[pointsCount - 1]));
                    return;
                }
                while (flag2) {
                    // 8 计算除去最坏点外的k-1个顶点的中心
                    getCenterPointExcludeWorstPoint(dimensionCount, pointsCount, x, xC);
                    // 9 判断除去最坏点外的所有k-1个顶点的中心是否可行
                    if (!isContained(xC)) {
                        for (int i = 0; i < dimensionCount; i++) {
                            a[i] = x[feasiblePointsCount - 1][i];
                            b[i] = xC[i];
                        }
                        flag = false;
                        break;
                    }
                    double alpha = 1.3;
                    while (true) {
                        // 10 求反射点
                        for (int i = 0; i < dimensionCount; i++) {
                            xR[i] = xC[i] + alpha * (xC[i] - x[0][i]);
                        }
                        if (!isContained(xR)) {
                            alpha *= 0.5;
                            // xR为非可行点，将alpha缩小0.5倍
                            System.out.printf("alpha = %.10f\n", alpha);
                        } else if (f(xR) < f(x[0])) {
                            // xR为可行点，用 xR取代xH
                            System.arraycopy(xR, 0, x[0], 0, dimensionCount);
                            flag2 = false;
                            break;
                        } else if (alpha <= 1e-10) {
                            // xR为可行点，alpha小于给定精度，用次坏点xG代替最坏点xH
                            for (int i = 0; i < dimensionCount; i++) {
                                double tmp = x[0][i];
                                x[0][i] = x[1][i];
                                x[1][i] = tmp;
                            }
                            break;
                        } else
                            alpha *= 0.5;
                    }
                }
            }
        }
    }

    /**
     * 求除最坏点外所有点的中心点
     */
    private void getCenterPointExcludeWorstPoint(
            int dimensionCount, int pointsCount, double[][] x, double[] xC) {
        for (int i = 0; i < dimensionCount; i++) {
            xC[i] = 0;
        }
        for (int i = 1; i < pointsCount; i++) {  // 从第二个值开始
            for (int j = 0; j < dimensionCount; j++) {
                xC[j] += x[i][j];
            }
        }
        for (int i = 0; i < dimensionCount; i++) {
            xC[i] /= (pointsCount - 1);
        }
    }

    /**
     * 求可行点的中心点
     */
    private void getCenterPointOfFeasiblePoints(
            int dimensionCount, double[][] x, double[] xC, int feasiblePointsCount) {
        for (int i = 0; i < dimensionCount; i++) {
            xC[i] = 0;
        }
        for (int i = 0; i < feasiblePointsCount; i++) {
            for (int j = 0; j < dimensionCount; j++) {
                xC[j] += x[i][j];
            }
        }
        for (int i = 0; i < dimensionCount; i++) {
            xC[i] /= feasiblePointsCount;
        }
        for (int i = 0; i < dimensionCount; i++) {
            System.out.printf("xC_%d = %f, ", i + 1, xC[i]);
        }
        System.out.print("\n");
    }

    /**
     * 将不可行点可行化
     */
    private void setNoFeasiblePointsToFeasible(
            int dimensionCount, int pointsCount, double[][] x, double[] xC, int feasiblePointsCount) {
        for (int i = feasiblePointsCount; i < pointsCount; i++) {
            if (!isContained(x[i])) {
                for (int j = 0; j < dimensionCount; j++) {
                    x[i][j] = xC[j] + 0.5 * (x[i][j] - xC[j]);
                }
            }
        }
    }

    /**
     * 求剩余的初始复合形的顶点
     */
    private void getOtherPoints(
            int dimensionCount, int pointsCount, double[][] x, double[] a, double[] b) {
        for (int i = 1; i < pointsCount; i++) {
            for (int j = 0; j < dimensionCount; j++) {
                x[i][j] = x[0][j] + a[j] + Math.random() * (b[j] - a[j]);
            }
        }
    }

    /**
     * 求可行点个数，并将可行点放到数组前部
     */
    private int getFeasiblePointCountAndPutHead(
            int dimensionCount, int pointsCount, double[][] x) {
        int feasiblePointsCount = 0;
        for (int i = 0; i < pointsCount; i++) {
            if (isContained(x[i])) {
                feasiblePointsCount++;
            } else {
                int j;
                for (j = i + 1; j < pointsCount; j++) {
                    if (isContained(x[j])) {
                        feasiblePointsCount++;
                        for (int k1 = 0; k1 < dimensionCount; k1++) {
                            double temp = x[i][k1];
                            x[i][k1] = x[j][k1];
                            x[j][k1] = temp;
                        }
                        break;
                    }
                }
                if (j == pointsCount) {
                    break;
                }
            }
        }
        return feasiblePointsCount;
    }

    /**
     * 将数组元素按目标函数值降序排列
     */
    private void setElementsToDesOrderByTargetValue(int n, int k, double[][] x) {
        for (int i = 0; i < k - 1; i++) {
            for (int j = i + 1; j < k; j++) {
                if (f(x[i]) < f(x[j])) {
                    for (int k1 = 0; k1 < n; k1++) {
                        double tmp = x[i][k1];
                        x[i][k1] = x[j][k1];
                        x[j][k1] = tmp;
                    }
                }
            }
        }
    }

    /**
     * 求目标函数值
     */
    double f(double[] x) {
        return Math.PI / 16 * (4 + 3 * Math.pow((4.64 - 2), 2)) * Math.pow(x[0], 2) * x[1] * Math.pow(x[2], 2);
    }


    /**
     * 求约束函数值
     */
    double[] g(double[] x) {
        final double K = 1.4;  // 载荷系数
        final double ZH = 2.5;  // 节点区域系数
        final double ZE = 189.8; // 弹性系数
        final double sigmaH = 0.9 * 565;  // 齿轮的接触疲劳许用应力,MPa
        double AH = Math.pow(2.32, 3) * K * Math.pow((ZH * ZE / sigmaH), 2);
        final double i = 4.64;  // 传动比
        final double T1 = 1118;  // 齿轮1的输入扭矩，Nm
        final double C = 3;  // 形星轮个数
        final double Ysa = 1.56;  // 应力校正系数
        final double sigmaF = 0.7 * 120;  // 许用弯曲应力
        double AF = 2 * K * (Ysa / sigmaF);
        double g1 = AH * T1 - Math.pow(x[0], 2) * x[1] * Math.pow(x[2], 2);
        double g2 = AF * T1 * (4.69 - 0.63 * Math.log(x[0])) - x[0] * x[1] * Math.pow(x[2], 2);
        double g3 = 2 - (i / 2 * Math.sin(Math.PI / C) - i / 2 + 1) * x[0];
        double g4 = 10 - x[1];
        double g5 = 2 - x[2];
        double g6 = 5 * x[2] - x[1];
        double g7 = x[1] - 17 * x[2];
        double g8 = 17 - x[0];
        return new double[]{g1, g2, g3, g4, g5, g6, g7, g8};
    }

    /**
     * 判断点是否在可行域内
     */
    boolean isContained(double[] x) {
        for (double res : g(x)) {
            if (res > 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        new OptimizedMethods().compoundOptimization();

        // do some test
//        final double x[] = {22.7345, 53.6821, 4, 4882};
//        double f = new OptimizedMethods().f(x);
//        System.out.println("f*(x)=" + f);
    }
}
